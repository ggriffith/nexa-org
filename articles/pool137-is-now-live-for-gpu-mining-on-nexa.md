---
layout: layout.html
id: '008'
title: Pool137 Is Now Live For GPU Mining On Nexa!!
draft: false
description: Pool137 has now launched a new GPU mining pool, making it even more accessible
  to join the Nexa mining community.
date: 5th December 2022
image: "/static/articles/poo137.png"
author: Bitcoin Unlimited
tags:
- Service
- Pool
- Mining

---
**Pool137** has now been relaunched as the first pool to allow GPU mining on the Nexa network. This now makes mining on Nexa even more accessible to the GPU mining community. You can now build a Nexa holding with just a single GPU.

Currently the pool only supports mining on a Linux operating system but Windows will follow shortly.

The pool takes a 1.75% fee and a 0.25% donation to go to Bitcoin Unlimited. This donation will be put towards the maintenance and growth of the Nexa network to make sure we achieve huge scale and reach the world's population.

**Get mining on the Pool137 right now at** [**https://nexapool.crypto137.com/**](https://nexapool.crypto137.com/ "https://nexapool.crypto137.com/")

You can also find a guide for getting up and running with the pool at [https://nexapool.crypto137.com/starting](https://nexapool.crypto137.com/starting "https://nexapool.crypto137.com/starting").

For any questions, make sure to get involved in the discussion on the #mining channel in our Discord community. [https://discord.gg/2yQNsZV6EJ](https://discord.gg/2yQNsZV6EJ "https://discord.gg/2yQNsZV6EJ")