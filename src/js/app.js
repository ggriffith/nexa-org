import Alpine from 'alpinejs'

// Load Alpine
window.Alpine = Alpine
Alpine.start()


// Custom JavaScript

// Menu

const menu      = document.getElementById("menu");
const openMenu  = document.getElementById("openMenu");
const closeMenu = document.getElementById("closeMenu");

openMenu.onclick = function () {menu.classList.remove("hidden");};
closeMenu.onclick = function () {menu.classList.add("hidden");};

// newsroom tags

const allToggleElems = document.querySelectorAll('.btn-tag-toggle');

allToggleElems.forEach(btnElem => {
  btnElem.addEventListener('click', e => {
    if (btnElem.classList.contains('active') && !btnElem.classList.contains('btn-tag-toggle-all')) {
      btnElem.classList.remove('active');
      btnElem.classList.remove('bg-white');
    } else {
      btnElem.classList.add('active');
      btnElem.classList.add('bg-white');
    }

    if (btnElem.classList.contains('btn-tag-toggle-all')) {
      allToggleElems.forEach(toggleElem => {
        if (!toggleElem.classList.contains('btn-tag-toggle-all')) {
          toggleElem.classList.remove('active');
          toggleElem.classList.remove('bg-white');
        }
      });
    } else {
      if (!document.querySelector('.btn-tag-toggle.active')) {
        document.querySelector('.btn-tag-toggle-all')?.classList.add('active');
        document.querySelector('.btn-tag-toggle-all')?.classList.add('bg-white');
      } else {
        document.querySelector('.btn-tag-toggle-all')?.classList.remove('active');
        document.querySelector('.btn-tag-toggle-all')?.classList.remove('bg-white');
      }
    }

    let showError = 1;

    if (document.querySelector('.btn-tag-toggle-all')?.classList.contains('active')) {
      showError = 0;
      document.querySelectorAll('.tag-news').forEach(tagElem => {
        tagElem.classList.remove('hidden');
      });
    } else {
      let tagActive = '';

      allToggleElems.forEach(toggleElem => {
        if (toggleElem.classList.contains('active')) {
          tagActive = tagActive.concat('.' + toggleElem.dataset.target);
        }
      });

      document.querySelectorAll('.tag-news').forEach(tagElem => {
        if (tagElem.matches(tagActive)) {
          showError = 0;
          tagElem.classList.remove('hidden');
        } else {
          tagElem.classList.add('hidden');
        }
      });
    }

    if (showError) {
      document.querySelector('.tag-error')?.classList.remove('hidden');
    } else {
      document.querySelector('.tag-error')?.classList.add('hidden');
    }
  });
});
